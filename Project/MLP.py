import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold
from sklearn.neural_network import MLPClassifier


def mlp(data):
    """
        Runs an MLPClassifier on the data.
        Plot loss and scores over all splits and if possible the loss curve of the classifier training

         Args:
                :param data: The data to run the MLPClassifier on
    """
    print("Running MLPClassifier")
    X = np.array(data[:, 0:(len(data[0]) - 1)])
    y = np.array(data[:, -1])

    loss_progression = []
    scores = []
    kf = KFold(n_splits=10)

    clf = MLPClassifier(solver='adam', hidden_layer_sizes=22, max_iter=750, learning_rate='constant')
    
    for train_index, test_index in kf.split(X):
        clf.fit(X[train_index], y[train_index])

        score = clf.score(X[test_index], y[test_index])
        loss = clf.loss_
        loss_progression.append(loss)
        scores.append(score)

    mlp_plot(clf, loss_progression, scores)

    accuracy = clf.score(X, y)
    print("Accuracy MLP", accuracy)

    return clf


def mlp_plot(clf, loss_progression, scores):
    """
    Plot loss and scores over all splits and if possible the loss curve of the classifier training

     Args:
            :param clf: The classifier to plot the loss of
            :param scores: The scores over the different splits
            :param loss_progression: The loss over the different splits
    """
    if clf.solver == 'sgd' or 'adam':
        fig = plt.figure(1, figsize=(8.5, 7))
        plt.plot(clf.loss_curve_)
        plt.xlabel("iteration")
        plt.ylabel("loss")
        plt.title("Loss curve of the training of the classifier")
        fig.savefig('img/ MLP loss training.png')
        plt.show()
    fig = plt.figure(1, figsize=(8.5, 7))
    plt.plot(scores, label="Scores")
    plt.plot(loss_progression, label="Loss")
    plt.xlabel("K-split")
    plt.ylabel("score/loss")
    plt.legend()
    fig.savefig('img/ MLP loss.png')
    plt.show()
