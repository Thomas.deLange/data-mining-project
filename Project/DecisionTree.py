import matplotlib.pyplot as plt
import numpy as np
from sklearn import metrics
from sklearn import tree
from sklearn.model_selection import KFold
from sklearn.tree import DecisionTreeClassifier


def decision_tree(data):
    """
    Runs an DTClassifier on the data. Prints the accuracy of the classification Performs k-fold cross-validation &
    shows a plot between maximum depth of the decision tree and the corresponding classification error

         Args:
                :param data: The data to run the DTClassifier on
    """

    print("Running DTClassifier")
    X = np.array(data[:, 0:(len(data[0]) - 1)])
    y = np.array(data[:, -1]).ravel()

    clf = tree.DecisionTreeClassifier(criterion="gini", min_samples_split=100)
    clf = clf.fit(X, y)

    fig = plt.figure(1, figsize=(24, 32))
    tree.plot_tree(clf, class_names=['healthy', 'sick'], filled=True, impurity=True, proportion=True)
    fig.savefig('img/Decisiontree.png')
    plt.show()

    # classification accuracy
    accuracy = clf.score(X, y)
    print("Accuracy DT", accuracy)

    # applies k-fold cross validation
    kf = KFold(n_splits=10)
    error_model = []
    max_depth = 50
    depth = [i for i in range(2, max_depth)]

    for i in range(2, max_depth):
        clf = DecisionTreeClassifier(criterion="gini", max_depth=i)
        for train_index, test_index in kf.split(X):
            model = clf.fit(X[train_index], y[train_index])
        error_model.append(1 - metrics.accuracy_score(y[test_index], model.predict(X[test_index])))

    fig = plt.figure(1, figsize=(8.5, 7))
    plt.title('10-fold Cross-Validation')
    plt.plot(depth, error_model)
    plt.ylabel('Classification Error')
    plt.xlabel('Depth')
    fig.savefig('img/ Kfold Decisiontree.png')
    plt.show()
    # print('Figure 3: A plot between maximum depth of the decision tree and the corresponding classification error
    # in 10-fold Cross-Validation')

    return clf
