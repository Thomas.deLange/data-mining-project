from Preprocessing import pre_processing
from MLP import mlp
import pandas as pd
from DecisionTree import decision_tree
from SVM import supportvectormachine


data = pd.read_csv('dataset_38_sick.csv')
data = pre_processing(data)
mlpClassifier = mlp(data)
dt_classifier = decision_tree(data)
svm_classifier = supportvectormachine(data)
