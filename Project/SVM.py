from sklearn import svm, metrics
import numpy as np
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt


def supportvectormachine(data):
    """
                Runs an SVMClassifier on the data.
                Performs k-fold cross-validation.
                Prints the accuracy of the classification.
                 Args:
                        :param data: The data to run the DTClassifier on
        """
    print("Running SVMClassifier")
    X = np.array(data[:, 0:(len(data[0]) - 1)])
    y = np.array(data[:, -1]).ravel()

    clf = svm.SVC()
    clf = clf.fit(X, y)
    error_model = []

    kf = KFold(n_splits=10)

    for train_index, test_index in kf.split(X):
        clf.fit(X[train_index], y[train_index])
        error_model.append(1 - metrics.accuracy_score(y[test_index], clf.predict(X[test_index])))

    fig = plt.figure(1, figsize=(8.5, 7))
    plt.title('10-fold Cross-Validation')
    plt.plot(error_model)
    plt.ylabel('Classification Error')
    fig.savefig('img/ Kfold SVM.png')
    plt.show()

    accuracy = clf.score(X, y)
    print("Accuracy SVM", accuracy)

    return clf
