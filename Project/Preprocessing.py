from sklearn import preprocessing
import numpy as np


def pre_processing(data):
    data = data.drop(['TBG', 'referral_source'], axis=1) # Drops uninteresting variables
    data = data.to_numpy()
    shape = data.shape
    data = data.ravel()

    # Changes all binary variables to either a 1 or 0
    for i, x in enumerate(data):
        if x in {'f', 'F', 'negative'}:
            x = 0
        elif x in {'t', 'M', 'sick'}:
            x = 1
        elif x == '?':
            x = np.nan
        data[i] = x

    # Removes any rows with NaN values
    data = [float(x) for x in data]
    data = np.array(data).reshape(shape)
    data = data[~np.isnan(data).any(axis=1)]

    # Normalizes the data
    scaler = preprocessing.StandardScaler()
    scaler.fit(data)

    print('data processed')
    return data
